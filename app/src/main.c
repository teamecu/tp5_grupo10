/* Copyright 2015, Pablo Ridolfi
 *
 * This file is part of TD2-Template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief This is a simple C example file.
 **
 **/

/** \addtogroup TD2 Técnicas Digitales II
 ** @{ */

/** @addtogroup App Aplicación de usuario
 * 	@{
 */

/*
 * Initials     Name
 * ---------------------------
 * PR           Pablo Ridolfi
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20150421 v0.0.1   PR first version
 */

/*==================[inclusions]=============================================*/

#include "FreeRTOS.h"
#include "board.h"
#include "main.h"
#include "task.h"

//#include <cr_section_macros.h>
#ifndef CR_SECTION_MACROS_H_INCLUDED
#define CR_SECTION_MACROS_H_INCLUDED

#define __SECTION_EXT(type, bank, name) __attribute__ ((section("." #type ".$" #bank "." #name)))
#define __SECTION(type, bank) __attribute__ ((section("." #type ".$" #bank)))
#define __SECTION_SIMPLE(type) __attribute__ ((section("." #type)))

#define __DATA_EXT(bank, name) __SECTION_EXT(data, bank, name)
#define __DATA(bank) __SECTION(data, bank)

#define __BSS_EXT(bank, name) __SECTION_EXT(bss, bank, name)
#define __BSS(bank) __SECTION(bss, bank)

#endif


#include "POWERANDPERIPHERALCLK.h"
#include "INTERRUPTS.h"
#include "GPIO.h"


#define VECTOR_LENGHT(x) (sizeof(x)/sizeof((x)[0]))

#define UPDATE_DAC_FREQUENCY(FREQUENCY,SIGNAL_SIZE) Chip_DAC_SetDMATimeOut((void*)LPC_DAC_BASE,(uint32_t)SystemCoreClock/(FREQUENCY*8*SIGNAL_SIZE))

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
    SystemCoreClockUpdate();

    Board_Init();

    Board_LED_Set(0, false);

    //SysTick_Config(SystemCoreClock/1000);
}

static void task(void * p)
{
   while (1)
   {
      Board_LED_Toggle(0);
      vTaskDelay(200 / portTICK_RATE_MS);
   }
}

/*==================[external functions definition]==========================*/

uint8_t dmaChannelNum;




//uint32_t sine[]= {32768,53824,65024,61120,43968,21568,4416,512,11712};
//uint32_t sine[] = {32768, 52032, 63936, 63936, 52032, 32768, 13504, 1600, 1600, 13504};
__DATA(RAM2) uint16_t sine[] = {32768, 34816, 36864, 38912, 40896, 42880, 44800, 46720, 48512, 50304, 51968, 53632, 55168, 56640, 57984, 59200, 60352, 61440, 62336, 63168, 63872, 64448, 64896, 65216, 65408, 65472, 65408, 65216, 64896, 64448, 63872, 63168, 62336, 61440, 60352, 59200, 57984, 56640, 55168, 53632, 51968, 50304, 48512, 46720, 44800, 42880, 40896, 38912, 36864, 34816, 32768, 30720, 28672, 26624, 24640, 22656, 20736, 18816, 17024, 15232, 13568, 11904, 10368,  8896,  7552,  6336,  5184,  4096,  3200,  2368,  1664,  1088,   640,   320,   128,    64,   128,   320,   640,  1088,  1664,  2368,  3200,  4096,  5184,  6336,  7552,  8896, 10368, 11904, 13568, 15232, 17024, 18816, 20736, 22656, 24640, 26624, 28672, 30720};
//100 points => Fmax =10khz
//__DATA(RAM2) uint32_t sine[] = {32768, 35968, 39168, 42240, 45312, 48192, 50944, 53504, 55872, 58048, 59968, 61632, 62976, 64064, 64832, 65344, 65472, 65344, 64832, 64064, 62976, 61632, 59968, 58048, 55872, 53504, 50944, 48192, 45312, 42240, 39168, 35968, 32768, 29568, 26368, 23296, 20224, 17344, 14592, 12032,  9664,  7488,  5568,  3904,  2560,  1472,   704,   192,    64,   192,   704,  1472,  2560,  3904,  5568,  7488,  9664, 12032, 14592, 17344, 20224, 23296, 26368, 29568};
//64 points
uint32_t sine_index = 0;
uint32_t frequency = 300;


GPDMA_CH_T * pDMAch;

int main(void)
{


	initHardware();


	//Chip_DAC_Init(LPC_DAC_BASE);//useless
	PCLKSEL_st.PCLK_DAC = Core_Clock_8;
	GPIO_InitPinNotAsGPIO(PORT_0, PIN_26,SECOND_ALT_FUNC,RES_NONE,NOT_OPEN_DRAIN);
	Chip_DAC_SetBias((void*)LPC_DAC_BASE, DAC_MAX_UPDATE_RATE_1MHz);
	//end of common initialization
	UPDATE_DAC_FREQUENCY(frequency, VECTOR_LENGHT(sine));



	Chip_GPDMA_Init((void*)LPC_GPDMA_BASE);
	dmaChannelNum = Chip_GPDMA_GetFreeChannel((void*)LPC_GPDMA, GPDMA_CONN_DAC);
	//pDMAch = (GPDMA_CH_T *) &((LPC_GPDMA)->CH[dmaChannelNum]);
	/*Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum,
							  _GPDMA_CONN_ADC,
							  (uint32_t) &DMAbuffer,
							  GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA,
							  1);*/
	INTERRUPT_SET_PRIORITY(IRQn_DMA, 5);
	INTERRUPT_CLEAR(IRQn_DMA);
	INTERRUPT_ENABLE(IRQn_DMA);


	/*Chip_GPDMA_Transfer((void*)LPC_GPDMA, dmaChannelNum,
								  (uint32_t) &sine[sine_index],
								  GPDMA_CONN_DAC,
								  GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA,
								  0);*/
	//Chip_GPDMA_ChannelCmd(LPC_GPDMA, dmaChannelNum, 0);

	//LPC_GPDMA->SYNC |= (1<<7);//probar con 7, que es DAC
			//pDMAch->CONTROL |= (0b101<<12);
			//pDMAch->CONTROL |= (0b1<<26);
			//pDMAch->CONTROL |= 100;
			//pDMAch->SRCADDR = (uint32_t)&sine[0];
	//pDMAch->CONTROL = VECTOR_LENGHT(sine)|(2<<18)|(2<<21)|(1<<26);


	DMA_TransferDescriptor_t lli_dac;
	Chip_GPDMA_PrepareDescriptor(LPC_GPDMA,&lli_dac,(uint32_t)&sine[0],GPDMA_CONN_DAC,VECTOR_LENGHT(sine),GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA,&lli_dac);
	lli_dac.dst = GPDMA_CONN_DAC;//bug GPDMA LIB
	Chip_GPDMA_SGTransfer(LPC_GPDMA,dmaChannelNum,&lli_dac,GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA);
	lli_dac.dst = (uint32_t)&(LPC_DAC->CR);//bug GPDMA LIB

	/*
	setupChannel(pGPDMA, &GPDMACfg, dsc->ctrl, dsc->lli, SrcPeripheral, DstPeripheral);
	Chip_GPDMA_ChannelCmd(pGPDMA, ChannelNum, ENABLE);*/
	/*lli_dac.src = (uint32_t)&sine[0];
	lli_dac.dst = (uint32_t)&(LPC_DAC->CR);
	lli_dac.ctrl = VECTOR_LENGHT(sine)|(2<<18)|(2<<21)|(1<<26);//32 bit src, 32bit dst, src increment
	lli_dac.lli = (uint32_t)&lli_dac;
	pDMAch->LLI = (uint32_t)&lli_dac;

	Chip_GPDMA_ChannelCmd(LPC_GPDMA, dmaChannelNum, ENABLE);*/
	Chip_DAC_ConfigDAConverterControl((void*)LPC_DAC_BASE,DAC_DBLBUF_ENA|DAC_CNT_ENA|DAC_DMA_ENA);
	//Chip_GPDMA_Transfer()
	//Chip_DAC_UpdateValue((void*)LPC_DAC_BASE, sine[sine_index++]>>6);//ver porque interrumpe antes de llegar aca


	while(1);
	//Chip_GPDMA_Stop(LPC_GPDMA, dmaChannelNum);
	//NVIC_DisableIRQ(DMA_IRQn);
	//end



	xTaskCreate(task, (signed const char *)"task", 1024, 0, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while(1);
}



void DMA_IRQHandler(void)
{
	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum) == SUCCESS) {
		/*Chip_GPDMA_Transfer((void*)LPC_GPDMA, dmaChannelNum,
											  (uint32_t) &sine[sine_index++],
											  GPDMA_CONN_DAC,
											  GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA,
											  1);*/
		/*pDMAch = (GPDMA_CH_T *) &((LPC_GPDMA)->CH[dmaChannelNum]);
		pDMAch->SRCADDR = (uint32_t) &sine[sine_index++];
		pDMAch->CONTROL = pDMAch->CONTROL | 1UL;
		pDMAch->CONFIG |= GPDMA_DMACCxConfig_E;*/

		//sine_index%=VECTOR_LENGHT(sine);
	}
	else {

		/* Process error here */
	}
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
