/*****************************************************************************************************//**
*
* @file		POWERANDPERIPHERALCLK.h
* @brief	Modulo para manejo del registro PCONP(peripheral power) Y PCLKSEL(peripheral CLK).
* @version	1.00
* @date		XX/XX/XXXX
* @author	gen13_V
*
*
*********************************************************************************************************
*** REVISION HISTORY
*
********************************************************************************************************/

/** @defgroup POWERANDPERIPHERALCLK POWERANDPERIPHERALCLK
 * @{
 */

/********************************************************************************************************
*** MODULE
********************************************************************************************************/
#ifndef POWER_PERIPHERALCLK_H_
#define POWER_PERIPHERALCLK_H_

/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/
#include "MY_LPC17xx.h"

/********************************************************************************************************
*** DEFINES
********************************************************************************************************/

//#define PCONP	    (*((__IO uint32_t *)0x400FC0C4UL))	/* Registro de Control de Alimentación de Periféricos */
#define PCONP_st	    (*((PCONP_struct *)0x400FC0C4UL))	/* Registro de Control de Alimentación de Periféricos */
#define PCLKSEL_st	(*((PCLKSEL_struct *)0x400FC1A8UL))	/* Registro de Selección de Clock de Periféricos */
#define CLKOUTCFG_st	(*((CLKOUTCFG_STRUCT *)0x400FC1C8UL))

/********************************************************************************************************
*** MACROS
********************************************************************************************************/


/********************************************************************************************************
*** DATA TYPES
********************************************************************************************************/

typedef enum{Core_Clock_4, Core_Clock_1, Core_Clock_2,Core_Clock_8} PCLK_Divider;

typedef struct{
		 uint32_t RESERVED0			:1;
	__IO uint32_t PCTIM0			:1;
	__IO uint32_t PCTIM1			:1;
	__IO uint32_t PCUART0			:1;
	__IO uint32_t PCUART1			:1;
		 uint32_t RESERVED1			:1;
	__IO uint32_t PCPWM1			:1;
	__IO uint32_t PCI2C0			:1;
	__IO uint32_t PCSPI				:1;
	__IO uint32_t PCRTC				:1;
	__IO uint32_t PCSSP1			:1;//bit 10
		 uint32_t RESERVED2			:1;
	__IO uint32_t PCADC				:1;
	__IO uint32_t PCCAN1			:1;
	__IO uint32_t PCCAN2			:1;
	__IO uint32_t PCGPIO			:1;
	__IO uint32_t PCRIT				:1;
	__IO uint32_t PCMCPWM			:1;
	__IO uint32_t PCQEI				:1;
	__IO uint32_t PCI2C1			:1;
		 uint32_t RESERVED3			:1;//bit 20
	__IO uint32_t PCSSP0			:1;
	__IO uint32_t PCTIM2			:1;
	__IO uint32_t PCTIM3			:1;
	__IO uint32_t PCUART2			:1;
	__IO uint32_t PCUART3			:1;
	__IO uint32_t PCI2C2			:1;
	__IO uint32_t PCI2S				:1;
		 uint32_t RESERVED4			:1;
	__IO uint32_t PCGPDMA			:1;
	__IO uint32_t PCENET			:1;//bit 30
	__IO uint32_t PCUSB				:1;

} PCONP_struct;

typedef struct{
	__IO uint32_t PCLK_WDT					:2;
	__IO uint32_t PCLK_TIMER0				:2;
	__IO uint32_t PCLK_TIMER1				:2;
	__IO uint32_t PCLK_UART0				:2;
	__IO uint32_t PCLK_UART1				:2;
		 uint32_t RESERVED0					:2;
	__IO uint32_t PCLK_PWM1					:2;
	__IO uint32_t PCLK_I2C0					:2;
	__IO uint32_t PCLK_SPI					:2;
		 uint32_t RESERVED1					:2;
	__IO uint32_t PCLK_SSP1					:2;//bit 20
	__IO uint32_t PCLK_DAC					:2;
	__IO uint32_t PCLK_ADC					:2;
	__IO uint32_t PCLK_CAN1					:2;
	__IO uint32_t PCLK_CAN2					:2;
	__IO uint32_t PCLK_ACF					:2;
	__IO uint32_t PCLK_QEI					:2;//2nd BYTE
	__IO uint32_t PCLK_GPIOINT				:2;
	__IO uint32_t PCLK_PCB					:2;
	__IO uint32_t PCLK_I2C1					:2;
		 uint32_t RESERVED2					:2;//bit 40
	__IO uint32_t PCLK_SSP0					:2;
	__IO uint32_t PCLK_TIMER2				:2;
	__IO uint32_t PCLK_TIMER3				:2;
	__IO uint32_t PCLK_UART2				:2;
	__IO uint32_t PCLK_UART3				:2;
	__IO uint32_t PCLK_I2C2					:2;
	__IO uint32_t PCLK_I2S					:2;
		 uint32_t RESERVED3					:2;
	__IO uint32_t PCLK_RIT					:2;
	__IO uint32_t PCLK_SYSCON				:2;//bit 60
	__IO uint32_t PCLK_MC					:2;

} PCLKSEL_struct;


typedef struct{
	__IO uint32_t SOURCE					:4;
	__IO uint32_t DIVISOR_0_TO_15			:4;
	__IO uint32_t ENABLE					:1;
	__IO uint32_t ACTIVITY_INDICATOR		:1;

} CLKOUTCFG_STRUCT;

typedef enum{
	CLKOUT_SOURCE_CPU = 0,
	CLKOUT_SOURCE_MAIN_OSCILLATOR = 1,
	CLKOUT_SOURCE_INTERNAL_RC_OSCILLATOR = 2,
	CLKOUT_SOURCE_USB = 3,
	CLKOUT_SOURCE_RTC = 4
} CLKOUT_SOURCE_ENUM;

/********************************************************************************************************
*** PUBLIC GLOBAL VARIABLES
********************************************************************************************************/


/********************************************************************************************************
*** PUBLIC FUNCTION PROTOTYPES
********************************************************************************************************/


/********************************************************************************************************
*** MODULE END
********************************************************************************************************/

#endif /* POWER_PERIPHERALCLK_H_ */

/**
 * @}
 */
