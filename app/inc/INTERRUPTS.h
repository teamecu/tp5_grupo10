/*****************************************************************************************************//**
*
* @file		I2C.h
* @brief	Module that handles interrupts.
* @version	1.00
* @date		XX/XX/XXXX
* @author	Nicolas Santamaria
*
*
*********************************************************************************************************
*** REVISION HISTORY
*
********************************************************************************************************/

/********************************************************************************************************
*** MODULE
********************************************************************************************************/

#ifndef INTERRUPTS_H_
#define INTERRUPTS_H_

/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/


/********************************************************************************************************
*** DEFINES
********************************************************************************************************/
//needed for the MACROS
#define ISER_REG (*((__IO uint32_t*)0xE000E100UL))
#define ICER_REG (*((__IO uint32_t*)0xE000E180UL))
#define ISPR_REG (*((__IO uint32_t*)0xE000E200UL))
#define ICPR_REG (*((__IO uint32_t*)0xE000E280UL))
#define IABR_REG (*((__I uint32_t*)0xE000E300UL))
#define IPR_REG (*((__IO uint32_t*)0xE000E400UL))

/********************************************************************************************************
*** MACROS
********************************************************************************************************/

/***************************************************************************//**
 * @fn			INTERRUPT_ENABLE()
 * @brief		Enables the selected interrupt.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_ENABLE(interrupt)  ((*(&ISER_REG + ((interrupt) / 32))) = (1 << (interrupt) % 32))

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		Disables the selected interrupt.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_DISABLE(interrupt) ((*(&ICER_REG + ((interrupt) / 32))) = (1 << (interrupt) % 32))

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		Changes the selected interrupt state to pending.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_SET(interrupt) 	 ((*(&ISPR_REG + ((interrupt) / 32))) = (1 << (interrupt) % 32))

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		Changes the selected interrupt state to not pending.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_CLEAR(interrupt)	 ((*(&ICPR_REG + ((interrupt) / 32))) = (1 << (interrupt) % 32))

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		This allows determining which peripherals are asserting an
 *				interrupt to the NVIC, and may also be pending if there are enabled.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_CHECK_ACTIVITY(interrupt) (((*(&IABR_REG + ((interrupt) / 32))) & (1 << (interrupt) % 32)) >> (interrupt) % 32 )

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		Checks if the desired interrupt is pending.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_CHECK_PENDING(interrupt)  (((*(&ISPR_REG + ((interrupt) / 32))) & (1 << (interrupt) % 32)) >> (interrupt) % 32 )

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		Checks if the desired interrupt is active.
 * @param[in]	interrupt	Interrupt request number.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_CHECK_STATE(interrupt)    (((*(&ISER_REG + ((interrupt) / 32))) & (1 << (interrupt) % 32)) >> (interrupt) % 32 )

/***************************************************************************//**
 * @fn			SMSSend()
 * @brief		Sets the desired interrupt priority being 0 the highest priority.
 * @param[in]	interrupt	Interrupt request number.
 * @param[in]	priority	Interrupt priority 0 to 31.
 * @return 		void
 ******************************************************************************/
#define INTERRUPT_SET_PRIORITY(interrupt, priority) ( (*(&IPR_REG + (((interrupt) * 8 + 3) / 32))) =\
		((*(&IPR_REG + (((interrupt) * 8 + 3) / 32))) &~ (0b1111 << (((interrupt) * 8+ 3) % 32))) | ((priority) << (((interrupt) * 8 + 3) % 32)) )

/********************************************************************************************************
*** DATA TYPES
********************************************************************************************************/

//Courtesy of CMSIS, with some modifications for simpler auto-completion.
typedef enum IRQn_enum{
/******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
	IRQn_NonMaskableInt           	= -14,      /*!< 2 Non Maskable Interrupt                         */
	IRQn_MemoryManagement         	= -12,      /*!< 4 Cortex-M3 Memory Management Interrupt          */
	IRQn_BusFault                	= -11,      /*!< 5 Cortex-M3 Bus Fault Interrupt                  */
	IRQn_UsageFault               	= -10,      /*!< 6 Cortex-M3 Usage Fault Interrupt                */
	IRQn_SVCall               		= -5,       /*!< 11 Cortex-M3 SV Call Interrupt                   */
	IRQn_DebugMonitor             	= -4,       /*!< 12 Cortex-M3 Debug Monitor Interrupt             */
	IRQn_PendSV                  	= -2,       /*!< 14 Cortex-M3 Pend SV Interrupt                   */
	IRQn_SysTick                  	= -1,       /*!< 15 Cortex-M3 System Tick Interrupt               */

/******  LPC17xx Specific Interrupt Numbers *******************************************************/
  IRQn_WDT_IRQn                     = 0,        /*!< Watchdog Timer Interrupt                         */
  IRQn_TIMER0                  		= 1,        /*!< Timer0 Interrupt                                 */
  IRQn_TIMER1                   	= 2,        /*!< Timer1 Interrupt                                 */
  IRQn_TIMER2                  		= 3,        /*!< Timer2 Interrupt                                 */
  IRQn_TIMER3                   	= 4,        /*!< Timer3 Interrupt                                 */
  IRQn_UART0                    	= 5,        /*!< UART0 Interrupt                                  */
  IRQn_UART1                    	= 6,        /*!< UART1 Interrupt                                  */
  IRQn_UART2                    	= 7,        /*!< UART2 Interrupt                                  */
  IRQn_UART3                  		= 8,        /*!< UART3 Interrupt                                  */
  IRQn_PWM1                     	= 9,        /*!< PWM1 Interrupt                                   */
  IRQn_I2C0                     	= 10,       /*!< I2C0 Interrupt                                   */
  IRQn_I2C1                    		= 11,       /*!< I2C1 Interrupt                                   */
  IRQn_I2C2                     	= 12,       /*!< I2C2 Interrupt                                   */
  IRQn_SPI                      	= 13,       /*!< SPI Interrupt                                    */
  IRQn_SSP0                    		= 14,       /*!< SSP0 Interrupt                                   */
  IRQn_SSP1                     	= 15,       /*!< SSP1 Interrupt                                   */
  IRQn_PLL0                     	= 16,       /*!< PLL0 Lock (Main PLL) Interrupt                   */
  IRQn_RTC                    		= 17,       /*!< Real Time Clock Interrupt                        */
  IRQn_EINT0                   		= 18,       /*!< External Interrupt 0 Interrupt                   */
  IRQn_EINT1                    	= 19,       /*!< External Interrupt 1 Interrupt                   */
  IRQn_EINT2                    	= 20,       /*!< External Interrupt 2 Interrupt                   */
  IRQn_EINT3                    	= 21,       /*!< External Interrupt 3 Interrupt                   */
  IRQn_ADC                     		= 22,       /*!< A/D Converter Interrupt                          */
  IRQn_BOD                      	= 23,       /*!< Brown-Out Detect Interrupt                       */
  IRQn_USB                     		= 24,       /*!< USB Interrupt                                    */
  IRQn_CAN                     		= 25,       /*!< CAN Interrupt                                    */
  IRQn_DMA                      	= 26,       /*!< General Purpose DMA Interrupt                    */
  IRQn_I2S                      	= 27,       /*!< I2S Interrupt                                    */
  IRQn_ENET                     	= 28,       /*!< Ethernet Interrupt                               */
  IRQn_RIT                      	= 29,       /*!< Repetitive Interrupt Timer Interrupt             */
  IRQn_MCPWM                   		= 30,       /*!< Motor Control PWM Interrupt                      */
  IRQn_QEI                     		= 31,       /*!< Quadrature Encoder Interface Interrupt           */
  IRQn_PLL1                    		= 32,       /*!< PLL1 Lock (USB PLL) Interrupt                    */
  IRQn_USBActivity              	= 33,       /* USB Activity interrupt                             */
  IRQn_CANActivity              	= 34,       /* CAN Activity interrupt                             */
} IRQn_ENUM;


/********************************************************************************************************
*** PUBLIC GLOBAL VARIABLES
********************************************************************************************************/


/********************************************************************************************************
*** PUBLIC FUNCTION PROTOTYPES
********************************************************************************************************/


/********************************************************************************************************
*** MODULE END
********************************************************************************************************/

#endif /* INTERRUPTS_H_ */
