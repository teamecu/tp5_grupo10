/*****************************************************************************************************//**
*
* @file		GPIO.h
* @brief	Módulo para manejo de Entradas y Salidas de Propósito General (GPIO: General Purpose IO).
* 			Este módulo es un complemento al desarrollado por NXP como librería y que esta incluido en
* 			este proyecto (lpc17xx_gpio.c y lpc17xx_gpio.h)
* @version	1.00
* @date		XX/XX/XXXX
* @author	Daniel J. López Amado (DJLA)
*
*
*********************************************************************************************************
*** REVISION HISTORY
*
********************************************************************************************************/


/********************************************************************************************************
*** MODULE
********************************************************************************************************/
#ifndef GPIO_H_
#define GPIO_H_


/********************************************************************************************************
*** INCLUDES
********************************************************************************************************/
#include "MY_LPC17xx.h"
//#include "LPC17xx.h"//dessulfator
#include "My_Types.h"


//dessulfator
typedef struct
{
  __IO uint32_t PINSEL0;
  __IO uint32_t PINSEL1;
  __IO uint32_t PINSEL2;
  __IO uint32_t PINSEL3;
  __IO uint32_t PINSEL4;
  __IO uint32_t PINSEL5;
  __IO uint32_t PINSEL6;
  __IO uint32_t PINSEL7;
  __IO uint32_t PINSEL8;
  __IO uint32_t PINSEL9;
  __IO uint32_t PINSEL10;
       uint32_t RESERVED0[5];
  __IO uint32_t PINMODE0;
  __IO uint32_t PINMODE1;
  __IO uint32_t PINMODE2;
  __IO uint32_t PINMODE3;
  __IO uint32_t PINMODE4;
  __IO uint32_t PINMODE5;
  __IO uint32_t PINMODE6;
  __IO uint32_t PINMODE7;
  __IO uint32_t PINMODE8;
  __IO uint32_t PINMODE9;
  __IO uint32_t PINMODE_OD0;
  __IO uint32_t PINMODE_OD1;
  __IO uint32_t PINMODE_OD2;
  __IO uint32_t PINMODE_OD3;
  __IO uint32_t PINMODE_OD4;
  __IO uint32_t I2CPADCFG;
} LPC_PINCON_TypeDef;

/*------------- General Purpose Input/Output (GPIO) --------------------------*/
typedef struct
{
  union {
    __IO uint32_t FIODIR;
    struct {
      __IO uint16_t FIODIRL;
      __IO uint16_t FIODIRH;
    };
    struct {
      __IO uint8_t  FIODIR0;
      __IO uint8_t  FIODIR1;
      __IO uint8_t  FIODIR2;
      __IO uint8_t  FIODIR3;
    };
  };
  uint32_t RESERVED0[3];
  union {
    __IO uint32_t FIOMASK;
    struct {
      __IO uint16_t FIOMASKL;
      __IO uint16_t FIOMASKH;
    };
    struct {
      __IO uint8_t  FIOMASK0;
      __IO uint8_t  FIOMASK1;
      __IO uint8_t  FIOMASK2;
      __IO uint8_t  FIOMASK3;
    };
  };
  union {
    __IO uint32_t FIOPIN;
    struct {
      __IO uint16_t FIOPINL;
      __IO uint16_t FIOPINH;
    };
    struct {
      __IO uint8_t  FIOPIN0;
      __IO uint8_t  FIOPIN1;
      __IO uint8_t  FIOPIN2;
      __IO uint8_t  FIOPIN3;
    };
  };
  union {
    __IO uint32_t FIOSET;
    struct {
      __IO uint16_t FIOSETL;
      __IO uint16_t FIOSETH;
    };
    struct {
      __IO uint8_t  FIOSET0;
      __IO uint8_t  FIOSET1;
      __IO uint8_t  FIOSET2;
      __IO uint8_t  FIOSET3;
    };
  };
  union {
    __O  uint32_t FIOCLR;
    struct {
      __O  uint16_t FIOCLRL;
      __O  uint16_t FIOCLRH;
    };
    struct {
      __O  uint8_t  FIOCLR0;
      __O  uint8_t  FIOCLR1;
      __O  uint8_t  FIOCLR2;
      __O  uint8_t  FIOCLR3;
    };
  };
} LPC_GPIO_TypeDef;

typedef struct
{
  __I  uint32_t IntStatus;
  __I  uint32_t IO0IntStatR;
  __I  uint32_t IO0IntStatF;
  __O  uint32_t IO0IntClr;
  __IO uint32_t IO0IntEnR;
  __IO uint32_t IO0IntEnF;
       uint32_t RESERVED0[3];
  __I  uint32_t IO2IntStatR;
  __I  uint32_t IO2IntStatF;
  __O  uint32_t IO2IntClr;
  __IO uint32_t IO2IntEnR;
  __IO uint32_t IO2IntEnF;
} LPC_GPIOINT_TypeDef;

#ifndef __CHIP_LPC175X_6X_H_

#define LPC_GPIO0_BASE        (LPC_GPIO_BASE + 0x00000)
#define LPC_GPIO1_BASE        (LPC_GPIO_BASE + 0x00020)
#define LPC_GPIO2_BASE        (LPC_GPIO_BASE + 0x00040)
#define LPC_GPIO3_BASE        (LPC_GPIO_BASE + 0x00060)
#define LPC_GPIO4_BASE        (LPC_GPIO_BASE + 0x00080)

#define LPC_GPIOINT_BASE      (LPC_APB0_BASE + 0x28080)
#define LPC_PINCON_BASE       (LPC_APB0_BASE + 0x2C000)

#define LPC_FLASH_BASE        (0x00000000UL)
#define LPC_RAM_BASE          (0x10000000UL)
#define LPC_GPIO_BASE         (0x2009C000UL)
#define LPC_APB0_BASE         (0x40000000UL)
#define LPC_APB1_BASE         (0x40080000UL)
#define LPC_AHB_BASE          (0x50000000UL)
#define LPC_CM3_BASE          (0xE0000000UL)

#define LPC_GPIOINT           ((LPC_GPIOINT_TypeDef   *) LPC_GPIOINT_BASE  )
#define LPC_PINCON            ((LPC_PINCON_TypeDef    *) LPC_PINCON_BASE   )

#endif









/********************************************************************************************************
*** DEFINES
********************************************************************************************************/
// Definición de Puertos
#define PORT_0		 0
#define PORT_1		 1
#define PORT_2		 2
#define PORT_3		 3
#define PORT_4		 4

// Definición de Pines
#define PIN_0		 0
#define PIN_1		 1
#define PIN_2		 2
#define PIN_3		 3
#define PIN_4		 4
#define PIN_5		 5
#define PIN_6		 6
#define PIN_7		 7
#define PIN_8		 8
#define PIN_9		 9
#define PIN_10		10
#define PIN_11		11
#define PIN_12		12
#define PIN_13		13
#define PIN_14		14
#define PIN_15		15
#define PIN_16		16
#define PIN_17		17
#define PIN_18		18
#define PIN_19		19
#define PIN_20		20
#define PIN_21		21
#define PIN_22		22
#define PIN_23		23
#define PIN_24		24
#define PIN_25		25
#define PIN_26		26
#define PIN_27		27
#define PIN_28		28
#define PIN_29		29
#define PIN_30		30
#define PIN_31		31


/********************************************************************************************************
*** MACROS
********************************************************************************************************/
/***************************************************************************//**
 * @fn			GPIO_GETpINsTATE()
 * @brief		Retorna el Estado del Pin pasado como parámetro teniendo en
 *              cuenta su Nivel de Actividad
 * @param[in]	portNum		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	pinNum		Número de Pin: Debe ser uno de los siguientes:
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in] 	activity	Nivel de Actividad: Debe ser uno de los tipos
 *                          definidos en la enumeración PIN_ACTIVITY
 * @return		Estado del Pin. Es uno de los tipos definidos en la enumeración DIGITAL_STATE
 ******************************************************************************/
#define GPIO_GETpINsTATE(portNum, pinNum, activity)			((activity == ACTIVE_HIGH) ? \
		                                            		((DIGITAL_STATE)((((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIOPIN & (0x01 << pinNum)) >> pinNum)) : \
		                                            		((DIGITAL_STATE)!((((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIOPIN & (0x01 << pinNum)) >> pinNum)))

/***************************************************************************//**
 * @fn			GPIO_SETpINsTATE()
 * @brief		Setea el Estado del Pin pasado como parámetro segun se indica:
 *              STATE_ON  && ACTIVE_HIGH ==> Pone un 1 en el Pin
 *              STATE_ON  && ACTIVE_LOW  ==> Pone un 0 en el Pin
 *              STATE_OFF && ACTIVE_HIGH ==> Pone un 0 en el Pin
 *              STATE_OFF && ACTIVE_LOW  ==> Pone un 1 en el Pin
 * @param[in]	portNum		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	pinNum		Número de Pin: Debe ser uno de los siguientes:
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in] 	activity	Nivel de Actividad: Debe ser uno de los tipos
 *                          definidos en la enumeración PIN_ACTIVITY
 * @param[in]	state	 	Estado del Pin: Valores posibles
 *			                - STATE_OFF
 *          			    - STATE_ON
 * @return		void
 ******************************************************************************/
#define GPIO_SETpINsTATE(portNum, pinNum, activity, state) 	((state == STATE_ON) ? \
		                                                   	GPIO_SETpIN(portNum, pinNum, activity) : \
		                                                   	GPIO_CLRpIN(portNum, pinNum, activity))

/***************************************************************************//**
 * @fn			GPIO_SETpIN()
 * @brief		Pone a STATE_ON el Estado del Pin pasado como parámetro segun se indica:
 *              ACTIVE_HIGH ==> Pone un 1 en el Pin
 *              ACTIVE_LOW  ==> Pone un 0 en el Pin
 * @param[in]	portNum		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	pinNum		Número de Pin: Debe ser uno de los siguientes:
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in] 	activity	Nivel de Actividad: Debe ser uno de los tipos
 *                          definidos en la enumeración PIN_ACTIVITY
 * @return		void
 ******************************************************************************/
#define GPIO_SETpIN(portNum, pinNum, activity) 				((activity == ACTIVE_HIGH) ? \
		                                            		(((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIOSET |= (0x01 << pinNum)) : \
		                                            		(((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIOCLR |= (0x01 << pinNum)))

/***************************************************************************//**
 * @fn			GPIO_CLRpIN()
 * @brief		Pone a STATE_OFF el Estado del Pin pasado como parámetro segun se indica:
 *              ACTIVE_HIGH ==> Pone un 0 en el Pin
 *              ACTIVE_LOW  ==> Pone un 1 en el Pin
 * @param[in]	portNum		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	pinNum		Número de Pin: Debe ser uno de los siguientes:
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in] 	activity	Nivel de Actividad: Debe ser uno de los tipos
 *                          definidos en la enumeración PIN_ACTIVITY
 * @return		void
 ******************************************************************************/
#define GPIO_CLRpIN(portNum, pinNum, activity) 				((activity == ACTIVE_HIGH) ? \
		                                            		(((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIOCLR |= (0x01 << pinNum)) : \
		                                            		(((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIOSET |= (0x01 << pinNum)))

/***************************************************************************//**
 * @fn			GPIO_SETpINdIR()
 * @brief		Setea la dirección del Pin pasado como parámetro:
 * @param[in]	portNum		Número de Puerto: Debe ser uno de los siguientes:
 *                          - PORT_0
 *                          - PORT_1
 *                          - PORT_2
 *                          - PORT_3
 * @param[in]	pinNum		Número de Pin: Debe ser uno de los siguientes:
 *                          - Desde PIN_0 hasta PIN_31
 * @param[in] 	dir 		Dirección del Pin: Debe ser uno de los tipos
 *                          definidos en la enumeración PIN_DIRECTION
 * @return		void
 ******************************************************************************/
#define GPIO_SETpINdIR(portNum, pinNum, dir) 				(((PIN_DIRECTION)dir == (PIN_DIRECTION)DIR_IN) ? \
		                                            		(((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIODIR &= ~(0x01 << pinNum)) : \
		                                            		(((LPC_GPIO_TypeDef *)LPC_GPIO_BASE+portNum)->FIODIR |= (0x01 << pinNum)))


/********************************************************************************************************
*** DATA TYPES
********************************************************************************************************/
typedef enum {RES_PULL_UP = 0, RES_LAST_STATE = 1, RES_NONE = 2, RES_PULL_DOWN = 3} PIN_RES_MODE;
typedef enum {NOT_OPEN_DRAIN = 0, OPEN_DRAIN = 1} PIN_OD_MODE;
typedef enum {ACTIVE_LOW = 0, ACTIVE_HIGH = 1} PIN_ACTIVITY ;
typedef enum {DAFAULT_FUNC = 0, FIRST_ALT_FUNC = 1, SECOND_ALT_FUNC = 2, THIRD_ALT_FUNC = 3} PIN_FUNCTION;
typedef enum {DIR_IN = 0, DIR_OUT = 1} PIN_DIRECTION;


typedef struct{
	uint8_t PortNum;
	uint8_t PinNum;
	PIN_DIRECTION Direction;
	PIN_RES_MODE ResistorMode;
	PIN_OD_MODE OpenDrainMode;
	PIN_ACTIVITY Activity;
}GPIO_STRUCT;

/********************************************************************************************************
*** PUBLIC GLOBAL VARIABLES
********************************************************************************************************/


/********************************************************************************************************
*** PUBLIC FUNCTION PROTOTYPES
********************************************************************************************************/
void GPIO_InitPinAsGPIO(uint8_t portNum, uint8_t pinNum, PIN_DIRECTION dir, PIN_RES_MODE resMode, PIN_OD_MODE odMode);
void GPIO_InitPinNotAsGPIO(uint8_t portNum, uint8_t pinNum, PIN_FUNCTION func, PIN_RES_MODE resMode, PIN_OD_MODE odMode);
void GPIO_SetPinFunc(uint8_t portNum, uint8_t pinNum, PIN_FUNCTION func);
void GPIO_SetResistorMode ( uint8_t portNum, uint8_t pinNum, PIN_RES_MODE resMode);
void GPIO_SetOpenDrainMode( uint8_t portNum, uint8_t pinNum, PIN_OD_MODE odMode);


/********************************************************************************************************
*** MODULE END
********************************************************************************************************/

#endif
