/*
 * LPC17xx.h
 */

#ifndef LPC17xx_H_
#ifndef __CHIP_LPC175X_6X_H_
#define LPC17xx_H_

#include <stdint.h>

#define     __I     volatile const     /*!< defines 'read only' permissions                 */
#define     __O     volatile           /*!< defines 'write only' permissions                */
#define     __IO    volatile           /*!< defines 'read / write' permissions              */



/******************************************************************************/
/*                 Core register					 					      */
 /*****************************************************************************/

/*------------- SysTick ------------------------------------------------------*/
//typedef struct
//{
//  __IO uint32_t CTRL;                    /*!< Offset: 0x000 (R/W)  SysTick Control and Status Register */
//  __IO uint32_t LOAD;                    /*!< Offset: 0x004 (R/W)  SysTick Reload Value Register       */
//  __IO uint32_t VAL;                     /*!< Offset: 0x008 (R/W)  SysTick Current Value Register      */
//  __I  uint32_t CALIB;                   /*!< Offset: 0x00C (R/ )  SysTick Calibration Register        */
//} SysTick_Type;

//#define SysTick_BASE		(0xE000E010UL)        	           		/*!< SysTick Base Address              	 */
#define SysTick             ((SysTick_Type *)       SysTick_BASE)   /*!< SysTick configuration struct        */
#define	STCTRL				SysTick->CTRL							/*!< SysTick Control and Status Register */
#define	STRELOAD			SysTick->LOAD							/*!< SysTick Reload Value Register       */
#define	STCURR				SysTick->VAL							/*!< SysTick Current Value Register      */
#define	STCALIB				SysTick->CALIB							/*!< SysTick Calibration Register        */


/*********************************************************************//**
 * Macro defines for System Timer Control and status (STCTRL) register
 **********************************************************************/
#define ST_CTRL_ENABLE		((uint32_t)(1<<0))
#define ST_CTRL_TICKINT		((uint32_t)(1<<1))
#define ST_CTRL_CLKSOURCE	((uint32_t)(1<<2))
#define ST_CTRL_COUNTFLAG	((uint32_t)(1<<16))

/*------------- NVIC ---------------------------------------------------------*/
#define ISER_BASE			(0xE000E100UL)
#define ISER0				(*(__IO uint32_t *)ISER_BASE)


/******************************************************************************/
/*                Device Specific Peripheral registers structures             */
/******************************************************************************/


/*------------- System Control (SC) ------------------------------------------*/


/*------------- Pin Connect Block (PINCON) -----------------------------------*/
/*typedef struct
{
  __IO uint32_t PINSEL0;
  __IO uint32_t PINSEL1;
  __IO uint32_t PINSEL2;
  __IO uint32_t PINSEL3;
  __IO uint32_t PINSEL4;
  __IO uint32_t PINSEL5;
  __IO uint32_t PINSEL6;
  __IO uint32_t PINSEL7;
  __IO uint32_t PINSEL8;
  __IO uint32_t PINSEL9;
  __IO uint32_t PINSEL10;
       uint32_t RESERVED0[5];
  __IO uint32_t PINMODE0;
  __IO uint32_t PINMODE1;
  __IO uint32_t PINMODE2;
  __IO uint32_t PINMODE3;
  __IO uint32_t PINMODE4;
  __IO uint32_t PINMODE5;
  __IO uint32_t PINMODE6;
  __IO uint32_t PINMODE7;
  __IO uint32_t PINMODE8;
  __IO uint32_t PINMODE9;
  __IO uint32_t PINMODE_OD0;
  __IO uint32_t PINMODE_OD1;
  __IO uint32_t PINMODE_OD2;
  __IO uint32_t PINMODE_OD3;
  __IO uint32_t PINMODE_OD4;
  __IO uint32_t I2CPADCFG;
} LPC_PINCON_TypeDef;*/

//#define LPC_PINCON            ((LPC_PINCON_TypeDef    *) 0x4002C000UL    )

/*------------- General Purpose Input/Output (GPIO) --------------------------*/
/*typedef struct
{
  __IO uint32_t FIODIR;
  	   uint32_t RESERVED0[3];
  __IO uint32_t FIOMASK;
  __IO uint32_t FIOPIN;
  __IO uint32_t FIOSET;
  __O  uint32_t FIOCLR;
} LPC_GPIO_TypeDef;*/

#define LPC_GPIO_BASE         (0x2009C000UL)
/*#define LPC_GPIO0             ((LPC_GPIO_TypeDef      *) 0x2009C000UL    )
#define LPC_GPIO1             ((LPC_GPIO_TypeDef      *) 0x2009C020UL    )
#define LPC_GPIO2             ((LPC_GPIO_TypeDef      *) 0x2009C040UL    )
#define LPC_GPIO3             ((LPC_GPIO_TypeDef      *) 0x2009C060UL    )
#define LPC_GPIO4             ((LPC_GPIO_TypeDef      *) 0x2009C080UL    )*/

/*#define IntStatus	(*((__I  uint32_t *)0x40028080UL))
#define IO0IntStatR	(*((__I  uint32_t *)0x40028084UL))
#define IO0IntStatF	(*((__I  uint32_t *)0x40028088UL))
#define IO0IntClr	(*((__O  uint32_t *)0x4002808CUL))
#define IO0IntEnR	(*((__IO uint32_t *)0x40028090UL))
#define IO0IntEnF	(*((__IO uint32_t *)0x40028094UL))
#define IO2IntStatR	(*((__I  uint32_t *)0x400280A4UL))
#define IO2IntStatF	(*((__I  uint32_t *)0x400280A8UL))
#define IO2IntClr	(*((__O  uint32_t *)0x400280ACUL))
#define IO2IntEnR	(*((__IO uint32_t *)0x400280B0UL))
#define IO2IntEnF	(*((__IO uint32_t *)0x400280B4UL))*/

/*------------- Timer (TIM) --------------------------------------------------*/

/*------------- Pulse-Width Modulation (PWM) ---------------------------------*/

/*------------- Universal Asynchronous Receiver Transmitter (UART) -----------*/

/*------------- Serial Peripheral Interface (SPI) ----------------------------*/

/*------------- Synchronous Serial Communication (SSP) -----------------------*/

/*------------- Inter-Integrated Circuit (I2C) -------------------------------*/

/*------------- Inter IC Sound (I2S) -----------------------------------------*/

/*------------- Repetitive Interrupt Timer (RIT) -----------------------------*/

/*------------- Real-Time Clock (RTC) ----------------------------------------*/

/*------------- Watchdog Timer (WDT) -----------------------------------------*/

/*------------- Analog-to-Digital Converter (ADC) ----------------------------*/

/*------------- Digital-to-Analog Converter (DAC) ----------------------------*/

/*------------- Motor Control Pulse-Width Modulation (MCPWM) -----------------*/

/*------------- Quadrature Encoder Interface (QEI) ---------------------------*/

/*------------- Controller Area Network (CAN) --------------------------------*/

/*------------- General Purpose Direct Memory Access (GPDMA) -----------------*/

/*------------- Universal Serial Bus (USB) -----------------------------------*/

/*------------- Ethernet Media Access Controller (EMAC) ----------------------*/

#endif
#endif /* LPC17xx_H_ */
